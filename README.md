# The SAP SQL Anywhere PHP Module

This is a fork of
[the SAP SQL Anywhere PHP Module][SASQLPHP]
with support for PHP 7.3.

[SASQLPHP]: https://wiki.scn.sap.com/wiki/display/SQLANY/The+SAP+SQL+Anywhere+PHP+Module

## Building

1. Ensure you have PHP development libraries installed.
   On Debian and derivatives, they're in the `php7.3-dev` package.
2. Clone this repository.
3. In your local clone, run `phpize`.
4. Run `./configure`.
5. Run `make`.

## Installing

### Linux (Debian and derivatives)

1. Copy `modules/sqlanywhere.so` into your `extension_dir`
   (as indicated by `php_info()`).
2. Create `/etc/php/7.3/mods-available/sqlanywhere.ini`:

        extension=sqlanywhere.so

3. Symlink it into the configuration directories:

        # for target in apache2 cli; do ln -s /etc/php/7.3/mods-available/sqlanywhere.ini /etc/php/7.3/"$target"/conf.d/; done

You'll need to restart Apache
to load the extension.
